      var dataNames = ['Name','Price (USD)','Operating system','Dimensions (mm)','Weight (kg)','Transport weight (kg)','Processor','Processor speed (GHz)','Ram (GB)','Type of storage','Storage capacity (GB)'];
      /*Get data for selected product from the json file*/
      var getData = function(product){
        var prodData; 
        $.ajax({
            async: false, /*async has to be disabled to return the data*/
            url: "data/products.json",
            success: function(data) {
                prodData = data[product];
            }
        });
        return prodData;
      }
      /*Build the table rows for selected product*/
      var buildSpecs = function(product){
        var arr = '';
        var row;
        var i = 0;
        $.each(product, function(key, value){
            row = '<tr><td>'+ dataNames[i++] +'</td><td>'+ value +'</td></tr>';
            arr += row;
        });
        return arr;
      };

      var productTable = function(product){
        var productSpecs = getData(product);
        var generateTable = "<table class='table table-striped' id='"+ product +"'> <tbody>"+ buildSpecs(productSpecs) +"</tbody> </table>";
        return generateTable;
      };
      /*If user selects a product*/
      $("select").change(function() {
        var inputName = $(this).attr('name'); /*Eg. firstProduct*/
        var selectedProduct = $(this).find('option:selected')[0].value; /*Eg. acer1*/
        var productName = $(this).find('option:selected')[0].text; /*Eg. Acer Aspire E1-572*/
        var resultSelector = $('#userResults').find('#'+inputName);

        var generatedTable = productTable(selectedProduct); /*Generate table for the selected product*/
        
        /*Show product changer if more than 1 product*/
        if ($('table').length != 0){
          $('#itemChange').show(); 
          $('#itemChange').find('#passText').text(productName); /*pass the product changer the Product name*/
          if ($('#itemChange').is(':visible')){
            $('table').addClass('hide'); /*hide all the other tables*/
            $('.alert').remove(); /*remove all alerts*/
          }
        }


        /*Replace table if already exists*/
        if (!resultSelector.is(':empty')){
          resultSelector.empty();
        }
        /*Check if the product is not being compared already*/
        if ($('#'+selectedProduct).length == 0){
          resultSelector.append(generatedTable);
        } else {
          resultSelector.append('<div class="alert alert-warning text-center table" role="alert">Product already selected, please select a different one</div>'); /*class table to simplify product changer functions bellow*/
        }
      });

      
      $('#changeForward').click(function(){
        var notHidden = $(".table:not(.hide)"); /*select product table which is shown*/
        var nextTable = notHidden
            .parent()
            .next()
            .find('table')[0];
        if( $(nextTable).length != 0){ /*Check if next table exists*/
          $(nextTable).removeClass('hide'); 
          var newText = $(nextTable).find('td:contains("Name")').next().text(); /*get the product name*/
          $('#passText').text(newText);/*replace the text in the product changer*/
          notHidden.addClass('hide'); /*hide the displayed table*/
        }
      });

      $('#changeBack').click(function(){
        var notHidden = $(".table:not(.hide)"); /*select product table which is shown*/
        var prevTable = notHidden
            .parent()
            .prev()
            .find('table')[0];
        if( $(prevTable).length != 0){ /*Check if previous table exists*/
          $(prevTable).removeClass('hide'); 
          var newText = $(prevTable).find('td:contains("Name")').next().text();/*get the product name*/
          $('#passText').text(newText); /*replace the text in the product changer*/
          notHidden.addClass('hide'); /*hide the displayed table*/
        }
      });



     