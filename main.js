var s,
compareTool = {

  settings: {
    jsonData: json,
    // Dom elements
    elDropOne: document.getElementById('drop-one'),
    elDropTwo: document.getElementById('drop-two'),
    elDropThree: document.getElementById('drop-three'),
    elResultOne: document.getElementById('first-result'),
    elResultTwo: document.getElementById('second-result'),
    elResultThree: document.getElementById('third-result'),
    elTableOne: document.getElementById('first-product'),
    elTableTwo: document.getElementById('second-product'),
    elTableThree: document.getElementById('third-product'),
    elTitleOne: document.getElementById('first-product-title'),
    elTitleTwo: document.getElementById('second-product-title'),
    elTitleThree: document.getElementById('third-product-title'),
    // Generate an array to store selected option
    aSelIndex : [-1,-1,-1]
  },


  init: function() {
    s = this.settings;
    // Init populating the dropdowns
    this.populateDropDown(s.elDropOne);
    this.populateDropDown(s.elDropTwo);
    this.populateDropDown(s.elDropThree);
    // Init change status
    this.onchangeStatus();

    s.aElResult= [s.elResultOne,s.elResultTwo,s.elResultThree];
  },

  populateDropDown : function(el){
    for (var i = 0; i<s.jsonData.length; i++){
      var opt = document.createElement('option');
      opt.value = i;
      opt.innerHTML = json[i].Name;
      el.appendChild(opt);
    }
  },

  onchangeStatus : function(){
    
    // First dropdown behaviour
    s.elDropOne.onchange = function(){
      
      optVal = this.options[this.selectedIndex].value;
      if (optVal != 'select a product') {
        s.aSelIndex[0] = optVal;
      } else {
        s.aSelIndex[0] = -1;
      }
      //console.log(s.aSelIndex);

      compareTool.updateDropDowns();
      compareTool.populateTable(s.elTableOne,optVal);
      compareTool.populateTitle(s.elTitleOne,optVal);
    }
    
    // Second dropdown behaviour
    s.elDropTwo.onchange = function(){
      
      optVal = this.options[this.selectedIndex].value;
      if (optVal != 'select a product') {
        s.aSelIndex[1] = optVal;
      } else {
        s.aSelIndex[1] = -1;
      }
      //console.log(s.aSelIndex);

      compareTool.updateDropDowns();
      compareTool.populateTable(s.elTableTwo,optVal);
      compareTool.populateTitle(s.elTitleTwo,optVal);
    }
    
    // Third dropdown behaviour
    s.elDropThree.onchange = function(){
      
      optVal = this.options[this.selectedIndex].value;
      if (optVal != 'select a product') {
        s.aSelIndex[2] = optVal;
      } else {
        s.aSelIndex[2] = -1;
      }
      //console.log(s.aSelIndex);
      compareTool.updateDropDowns();
      compareTool.populateTable(s.elTableThree,optVal);
      compareTool.populateTitle(s.elTitleThree,optVal);
    }
  },

  updateDropDowns : function(){
    compareTool.enableAllOpts(s.elDropOne);
    compareTool.enableAllOpts(s.elDropTwo);
    compareTool.enableAllOpts(s.elDropThree);
    compareTool.disableOpts(s.elDropOne);
    compareTool.disableOpts(s.elDropTwo);
    compareTool.disableOpts(s.elDropThree);
  },

  enableAllOpts: function(el){
    var length = el.options.length;
    for (i = 0; i < length; i++) {
      el.options[i].removeAttribute('disabled');
    }
  },

  disableOpts : function(el){
    //console.log(el.options);
    for (var i=0; i<s.aSelIndex.length;i++){
      //console.log(el.options[s.aSelIndex[i]]);
      if(s.aSelIndex[i] != -1){
        el.options[parseInt(s.aSelIndex[i])+1].setAttribute('disabled','disabled');
      }
      
    }
  },

  populateTable: function(el,i){
    var tbody = el.getElementsByTagName('tbody')[0];
    tbody.innerHTML = '';
    // console.log(el);
    // console.log(s.jsonData[i]);
    var obj = s.jsonData[i];
    var tr = '';
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
         tr += '<tr><td>' +key +':<br> <strong>'+ obj[key]+ '</strong></td></tr>';
      }
    }
    // console.log(tr);
    tbody.innerHTML += tr;
  },

  populateTitle: function(el,i){
    el.innerHTML = s.jsonData[i].Name;
  },

  triggerChange : function (el,i){
    if (s.aSelIndex[i] == -1){
      el.selectedIndex = 1;  
    } else {
      el.selectedIndex = parseInt(s.aSelIndex[i]) + 1;  
    }
    
    var event = document.createEvent('HTMLEvents');
    event.initEvent('change', true, false);
    el.dispatchEvent(event);
  }, 

  prevIndex: function(el,i){
    var arrLength = s.jsonData.length;
    if (s.aSelIndex[i]==0){
      s.aSelIndex[i] = arrLength -1;  
    } else {
      s.aSelIndex[i] = parseInt(s.aSelIndex[i])-1;   
    }
    
    compareTool.triggerChange(el,i);
  },
  
  nextIndex: function(el,i){
    var arrLength = s.jsonData.length;
    if (s.aSelIndex[i] == arrLength -1){
      s.aSelIndex[i] = 0; 
    } else {
      s.aSelIndex[i] = parseInt(s.aSelIndex[i])+1;
    }
     
    compareTool.triggerChange(el,i); 
  },

  activateBtn: function(el,index){
    var className = 'active';
    // First remove class active to previous active button
    var x = document.getElementsByTagName('li');
    for (var i=0; i<x.length;i++){
      if (x[i].classList){
        x[i].classList.remove(className);
      } else {
        x[i].className = x[i].className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
      } 
    }

    for (var i=0; i<s.aElResult.length;i++){
      if (s.aElResult[i].classList){
        s.aElResult[i].classList.remove(className);
      } else {
        s.aElResult[i].className = s.aElResult[i].className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
      } 
    }
    
    // Then add class active to clicked button
    if (el.classList){
      el.classList.add(className);
    } else {
      el.className += ' ' + className;
    }

    switch(index){
      case 0:
        var c = s.elResultOne;
      break;
      case 1:
        var c = s.elResultTwo;
      break;
      case 2:
        var c = s.elResultThree;
      break;
    }

    if (c.classList){
      c.classList.add(className);
    } else {
      c.className += ' ' + className;
    }

  }

};

compareTool.init();